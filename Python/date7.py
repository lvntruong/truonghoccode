class Animal():
	noise = "Grunt"
	size = "Lagre"
	color = "brown"
	hair = "covers :v"
	def get_color(self):
		return self.color
	def make_noise(self):
		return self.noise

dog = Animal()
dog.make_noise()
dog.size = "small"
dog.hair = "toc"
dog.color = "black"

class Dog(Animal):
	name = 'Jon'

jon = Dog()
jon.color = "white"
jon.name = "JON :3"