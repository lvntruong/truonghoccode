https://laravel.com/docs/5.4
https://www.rosehosting.com/blog/install-laravel-on-ubuntu-16-04/
https://www.youtube.com/watch?v=Iqy4GZwxFrw&list=PLzrVYRai0riQ-K705397wDnlhhWu-gAUh&index=7
http://khoapham.vn/download/laravel/bai2.pdf
#################################### INSTALL #######################################
sudo su
apt update && apt upgrade
apt install php-mcrypt php-gd php-mbstring
curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer

composer global require "laravel/installer"
composer create-project --prefer-dist laravel/laravel blog

chmod +x /usr/local/bin/composer
mkdir /var/www/html/your_website
cd /var/www/html/your_website
git clone https://github.com/laravel/laravel.git
mv laravel/* .
rmdir laravel/
sudo chown -R $USER $HOME/.composer
composer install
chown www-data: -R /var/www/html/your_website/
mv .env.example .env
php artisan key:generate
nano config/app.php
'key' => env('APP_KEY', 'base64:7fO0S9TxZu8M2NwBWVEQsjPGRi+D1t6Ws8i0Y2yW/vE='),
php artisan serve
#################################### Virtual Host #######################################
Create an Apache virtual host file so your domain can serve Laravel. Open a file, for example your_website.conf:
nano /etc/apache2/sites-available/lavarel.conf

Paste the following:
<VirtualHost *:80>
    ServerAdmin admin@lavarel.truong
    ServerName lavarel.truong
    ServerAlias www.lavarel.truong
    DocumentRoot /var/www/html/hoang-truong-vinh/TruongLee/Lavarel/my_first_web/public
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

cd ../sites-enabled
sudo ln -s ../sites-available/lavarel.conf

Enable the site:
# a2ensite lavarel.truong
Restart Apache so the changes can take effect:
# service apache2 reload
##################################### ROUTING #########################################
Route::get('user/{name}', function ($name) {
    //
})->where('name', '[A-Za-z]+');

Route::get('user/{id}', function ($id) {
    //
})->where('id', '[0-9]+');

Route::get('user/{id}/{name}', function ($id, $name) {
    //
})->where(['id' => '[0-9]+', 'name' => '[a-z]+']);

Route::get('Route1',[
    'as' => 'MyRoute',
    function (){
        echo 'Xin Chao Cac Ban!';
    }
]);

Route::get('Route2', function(){
    echo 'Day la route 2';
})->name('MyRoute2');

Route::get('GoiTen', function (){
    return redirect()->route('MyRoute2');
});

// Share
View::share('titleShare', 'Title Demo');
##################################### Controller #########################################
php artisan make:controller MyController
Route::get('hello/{name?}', 'MyController@Hello');
##################################### FORM #########################################
<form action="{{route('postForm')}}" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="text" name="HoTen">
    <input type="submit">        
</form>

Route::post('postForm', ['as'=>'postForm', 'uses'=>'MyController@PostForm']);

public function PostForm(Request $request){
    echo $request->HoTen;
    echo json_encode($request->all());  
    echo json_encode($request->except('_token'));  
}
##################################### COOKIE #########################################
use Illuminate\Http\Request;
use Illuminate\Http\Response;

public function SetCookie(){
    $response = new Response();
    $response->withCookie('nameCookie','valueCookie',1);
    return $response;
}

public function GetCookie(Request $request){
    return $request->cookie('nameCookie');
}
##################################### FILE UPLOAD #########################################
<form action="{{route('uploadForm')}}" method="post" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <input type="file" name="myFile">
    <input type="submit">        
</form>

public function PostFile(Request $request){
    // return json_encode($request->all());
    if ($request->hasFile('myFile')){
        $file = $request->file('myFile');
        $fileExtension = $file->getClientOriginalExtension('myFile');
        if (($fileExtension == 'jpg') || ($fileExtension == 'png')){
            $fileName = $file->getClientOriginalName('myFile');
            $file->move('img', $fileName);
            echo "Da luu file : ".$fileName;
        } else {
            echo "Chi duoc phep upload jpg, png";
        }
    } else {
        echo 'Chua co file';
    }
}
##################################### Blade Template #########################################
Master.blade.php

<link rel="stylesheet" href="{{asset('css/style.css')}}">
@include('pages.header')
@yield(‘NoiDung’)
@include('pages.footer')

Home.blade.php

@extends(‘Master’)
@section(‘NoiDung’)
<a href=”ahihi.vn” > Laravel </a>
@endsection

################################ BLADE TEMPLATE ##############################################
$ten = "<b>Truong</b>";

{{$ten}} // Hien thi raw string
{!! $ten !!} // Hien thi html

// IF ELSE
@if($ten != "")
    {{$ten}}
    {!! $ten !!}
@else
    {{"Khong co ten"}}
@endif

{!! $ten or "Khong co bien ten" !!}
{{ $ten or "Khong co bien ten" }}

// FOR
@for ($i = 1; $i <=10; $i++)
    {{$i}}
@endfor

// FOR Mang
@forelse($arrayTest as $value)
    @continue($value=="java")
    @break($value=="html")
    {{$value}}
@empty
    {{"Mang rong"}}
@endforelse

################################ DATABASE ##############################################
################################ MIGRATE ##############################################
php artisan make:migration create_hocsinh_table --create=hocsinh

public function up()
{
    Schema::create('hocsinh', function (Blueprint $table) {
        $table->increments('id');
        $table->timestamps();
    });
}
public function down()
{
    Schema::dropIfExists('hocsinh');
}

php artisan make:migration create_monhoc_table --table=monhoc

php artisan migrate
php artisan migrate:rollback
php artisan migrate:refresh
################################ SEED ##############################################
php artisan db:seed

$this->call(UsersTableSeeder::class);

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            ['name'=> 'truong'.str_random(3),'email'=> str_random(10).'@gmail.com','password'=> bcrypt('secret')],
            ['name'=> 'dieu'.str_random(3),'email'=> str_random(10).'@gmail.com','password'=> bcrypt('secret')]
        ]);
    }
}
################################ Query Builder ##############################################
SELECT *
$data = DB::table('users')->get();
SELECT * WHERE
$data = DB::table('users')->where('id','=',$num)->get();
SELECT 'id','name','email' WHERE
$data = DB::table('users')->select(['id','name','email'])->where('id',$num)->get();
SELECT 'id','name' as HoTen,'email' WHERE
$data = DB::table('users')->select(DB::raw('id, name as hoten, email'))->get();
SELECT 'id','name' as HoTen,'email' WHERE ORDER BY
$data = DB::table('users')->select(DB::raw('id, name as hoten, email'))->orderby('id','desc')->get();
LIMIT 2,5 Skip(1)->take(5) Lay tu 2-5
UPDATE
$data = DB::table('users')->where('id',1)->update(['name'=>'Ahihi']);
INSERT
DB::table('users')->insert([...]);
TRUNCATE
$data = DB::table('users')->where('id',6)->truncate();

Route::get('qr/get',function(){
    $data = DB::table('users')->get();
    // var_dump($data);
    foreach ($data as $row){
        foreach ($row as $key=>$value){
            echo $key." : ".$value;
            echo "<br>";
        }
        echo "<hr>";
    }
});
################################ Model #############################################
Tao model
php artisan make:model TenModel
Tạo một model và migrate tương ứng với nó :
Php artisan make:model TenModel -m

CAUTION
public $timestamps = false;

$user = new User();
echo $user->name;
Lấy giá trị thuộc tính của model
$user = User::all(); Lấy toàn bộ dữ liệu trong bảng
$user = User::find(giá trị khóa chính); Tìm user theo khóa chính
$user->toJson(); Trả dữ liệu kiểu JSON
$user->save(); Lưu dữ liệu từ model vào bảng
$user->delete(); Xóa dữ liệu trong bảng
User::destroy(giá trị khóa chính); Xóa dữ liệu bằng khóa chính trong bảng
################################ Relationships ###########################################
public function TenLienKet()
{
 return $this->hasMany( ‘TenModel’ , ‘KhoaPhu’, ‘KhoaChinh’ );
}

uses: TenModel::TenLienKet()

Bảng liên kết
Liên kết Hàm liên kết
Một – Một , Liên kết từ bảng cha tới bảng con. hasOne();
Một – Một , Liên kết từ bảng con tới bảng cha. belongsTo();
Một – Nhiều hasMany();
Nhiều – nhiều belongsToMany();
Liên kết qua bảng trung gian hasManyThrough();

################################ MIDDLEWARE ###########################################
php artisan make:middleware TenMiddleware

public function handle($request, Closure $next)
{
    if($request->has('diem') && $request['diem'] > 5)
        return $next($request);
    else
        return redirect()->route('loi');
}

################################ AUTH ###########################################
use Illuminate\Support\Facades\Auth;

// $user = User::find(1);
// return view('thanhcong', ['user'=>Auth::user()]);

if(Auth::attempt(['name' => $username, 'password' => $password])){
    return view('thanhcong', ['user'=>Auth::user()]);
} else {
    return view('dangnhap',['error'=>'Dang nhap that bai']);
}


attempt(array()) Đăng nhập với thông tin đăng nhập
login(model) Đăng nhập với đối tượng
logout() Hủy đăng nhập
user() Lấy thông tin người đang đăng nhập
check() Kiểm tra đăng nhập chưa

################################ Session ###########################################
// Session:: hoac session()->
Route::get('session',function(){
    Session::put('Truong', 'DepTrai');
    echo 'Da dat session';
    echo '<br>';
    echo Session::get('Truong');
});

put('Ten','GiaTri'); Khai báo session
flash('flasha','Đây là session flash'); Khai báo flash
session('Ten'); Lấy dữ liệu từ session
has(‘Ten’) Kiểm tra session tồn tại không
forget('Ten'); Xóa bỏ 1 session
flush(); Xóa hết các session

################################ Pagination ###########################################
