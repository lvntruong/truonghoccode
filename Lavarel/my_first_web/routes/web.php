<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('ten/{ten}', function ($ten) {
    return 'Ten : '.$ten;
});

Route::get('user/{name?}', function ($name = 'Truong') {
    return $name;
});

Route::get('hello/{name?}', 'MyController@Hello');

// URL
Route::get('GoiRequest', 'MyController@GetURL');

// Gui nhan du lieu voi request
Route::get('getForm', function(){
    return view('postForm');
});

// Route::post('postForm', 'MyController@postForm')->name('postForm');
Route::post('postForm', ['as'=>'postForm', 'uses'=>'MyController@PostForm']);

// Cookie
Route::get('setCookie', 'MyController@SetCookie');
Route::get('getCookie', 'MyController@GetCookie');

// Upload file
Route::get('uploadFile', function() {
    return view('postFile');
});


Route::post('postFile', 'MyController@PostFile')->name('postFile');

// JSON
Route::get('getJson', 'MyController@GetJson');

// View
Route::get('myView', 'MyController@MyView');

Route::get('Time/{t?}','MyController@Time');

    // Share
View::share('titleShare', 'Title Demo');

// Blade template
Route::get('blade', function(){
    return view('pages.php');
});

Route::get('BladeTemplate/{str?}', 'MyController@Blade');

// Database
Route::get('database', function(){
    // Schema::create('loaisanpham', function ($table) {
    //     $table->increments('id');
    //     $table->string('ten',200);
    // });

    Schema::create('theloai', function ($table) {
        $table->increments('id');
        $table->string('ten',200)->nullable();
        $table->string('nsx')->default('Nha San Xuat');
    });
    echo "Đã tạo bảng";
});

Route::get('lienketbang', function(){
    Schema::create('sanpham', function ($table) {
        $table->increments('id');
        $table->string('ten');
        $table->float('gia');
        $table->integer('soluong')->default(0);
        $table->integer('id_loaisanpham')->unsigned();
        $table->foreign('id_loaisanpham')->references('id')->on('loaisanpham');
    });
    echo "Đã tạo bảng";  
});

Route::get('suabang', function(){
    Schema::table('theloai', function($table) {
        $table->dropColumn('nsx');
    });
});

Route::get('themcot', function(){
    Schema::table('theloai', function($table) {
        $table->string('email');
    });
    echo "Đã them cot email";      
});

Route::get('doiten', function(){
    Schema::rename('theloai','nguoidung');
    echo "Đã doi ten bane";      
});

Route::get('xoabang', function(){
    // Schema::drop('nguoidung');
    Schema::dropIfExists('nguoidung');
    echo "Đã xoa bang nguoidung";      
});

Route::get('taobang', function(){
    Schema::create('nguoidung', function ($table) {
        $table->increments('id');
        $table->string('ten',200);
    });
    echo "Đã tạo bảng";
});

// Query Builder
Route::get('qr/get',function(){
    $data = DB::table('users')->get();
    // var_dump($data);
    foreach ($data as $row){
        foreach ($row as $key=>$value){
            echo $key." : ".$value;
            echo "<br>";
        }
        echo "<hr>";
    }
});

Route::get('qr/where/{num?}',function($num){
    $data = DB::table('users')->where('id','=',$num)->get();
    // var_dump($data);
    foreach ($data as $row){
        foreach ($row as $key=>$value){
            echo $key." : ".$value;
            echo "<br>";
        }
        echo "<hr>";
    }
});

Route::get('qr/select/{num?}',function($num){
    $data = DB::table('users')->select(['id','name','email'])->where('id',$num)->get();
    // var_dump($data);
    foreach ($data as $row){
        foreach ($row as $key=>$value){
            echo $key." : ".$value;
            echo "<br>";
        }
        echo "<hr>";
    }
});

Route::get('qr/raw',function(){
    $data = DB::table('users')->select(DB::raw('id, name as hoten, email'))->get();
    // var_dump($data);
    foreach ($data as $row){
        foreach ($row as $key=>$value){
            echo $key." : ".$value;
            echo "<br>";
        }
        echo "<hr>";
    }
});

Route::get('qr/orderby',function(){
    $data = DB::table('users')->select(DB::raw('id, name as hoten, email'))->take(3)->orderby('id','desc')->get();
    foreach ($data as $row){
        foreach ($row as $key=>$value){
            echo $key." : ".$value;
            echo "<br>";
        }
        echo "<hr>";
    }
});

Route::get('qr/update',function(){
    $data = DB::table('users')->where('id',1)->update(['name'=>'Ahihi']);
    echo "Da update";
});

Route::get('qr/delete',function(){
    $data = DB::table('users')->where('id',6)->delete();
    echo "Da xoa";
});

Route::get('qr/truncate',function(){
    $data = DB::table('users')->where('id',6)->truncate();
    echo "Da xoa";
});

// model
Route::get('model/save', function(){
    $user = new App\User();
    $user->name = "truong".str_random(3);
    $user->email = "truong".str_random(3).'@gmail.com';
    $user->password = bcrypt('truong');    

    $user->save();
    echo "Da thuc hien save";
});

Route::get('model/query', function(){
    $user = App\User::find(1);;
    echo $user->name;
});

Route::get('model/sanpham/save/{soluong?}', function($soluong){
    for ($i=0; $i < $soluong ; $i++) { 
        $sanpham = new App\SanPham();
        $sanpham->ten = str_random(10);
        $sanpham->soluong = rand(10,100);
        $sanpham->id_loaisanpham = rand(1,2);
        $sanpham->save();
        echo "da thuc hien lenh save ".$sanpham->ten."<br>";
    }
});

Route::get('model/sanpham/all', function(){
    $sanpham = App\SanPham::all()->toJson();
    echo $sanpham;
});

Route::get('model/sanpham/ten', function(){
    $sanpham = App\SanPham::where('ten','HP')->get()->toJson();
    echo $sanpham;
});

Route::get('model/sanpham/destroy', function(){
    App\SanPham::destroy(4);
});

Route::get('taocot', function(){
    Schema::table('sanpham', function($table) {
        $table->integer('id_loaisanpham')->unsigned;
    });
});

Route::get('lienket', function(){
    $data = App\SanPham::find(3)->LoaiSanPham->toJson();
    echo $data;
});

Route::get('lienketloaisanpham', function(){
    $data = App\LoaiSanPham::find(1)->SanPham->toJson();
    echo $data;
});

// Middleware

Route::get('diem', function(){
    echo "Ban da co diem";
})->middleware('MyMiddle')->name('diem');

Route::get('loi', function(){
    echo "Ban chua co diem";
})->name('loi');

Route::get('nhapdiem', function(){
    return view('nhapdiem');
})->name('nhapdiem');

// Auth

Route::get('dangnhap', function(){
    return view('dangnhap');
});

Route::post('login', 'AuthController@login')->name('login');

Route::get('logout', 'AuthController@logout')->name('logout');

// Session
Route::get('session',function(){
    Session::put('Truong', 'DepTrai');
    echo 'Da dat session';
    echo '<br>';
    if(Session::has('Truong')) echo "da co session";
    else echo "chua co session";

    // Session::flash('Ahihi', "Nhin e mot lan thoi");
    session()->flash('Ahihi', "Nhin e mot lan thoi");
});

Route::get('session/flash', function(){
    // echo Session::get('Ahihi');
    echo session('Ahihi');
});

// Pagination

Route::get('sanpham', 'SanPham@index');