<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Test Post</title>
</head>
<body>
    {{ $error or ''}}
    <form action="{{route('login')}}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="text" name="username" placeholder="username">
        <input type="password" name="password" placeholder="******">
        <input type="submit">        
    </form>
</body>
</html>