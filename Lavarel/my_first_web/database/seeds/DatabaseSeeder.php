<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(UsersTableSeeder::class);

        // DB::table('users')->insert([
        //    'name'=> str_random(10),
        //    'email'=> str_random(10).'@gmail.com',
        //    'password'=> bcrypt('secret'),             
        // ]);
    }
}

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->insert([
            ['name'=> 'truong'.str_random(3),'email'=> str_random(10).'@gmail.com','password'=> bcrypt('secret')],
            ['name'=> 'dieu'.str_random(3),'email'=> str_random(10).'@gmail.com','password'=> bcrypt('secret')]
        ]);
    }
}