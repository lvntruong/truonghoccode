<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MyController extends Controller
{
    public function Hello($name = ''){
        echo "Hello world! ".$name;
    }

    public function GetURL(Request $request){
        return $request->url();

        // if ($request->isMethod('post')) echo 'Day la post';
        //     else echo 'Day khong phai la post';

        // if ($request->is('Goi*')) echo "Co Goi";
        //     else echo "Khong Co Goi";
    }

    public function PostForm(Request $request){
        // echo json_encode($request->all());
        echo json_encode($request->except('_token'));        
    }

    public function SetCookie(){
        $response = new Response();
        $response->withCookie('nameCookie','valueCookie',1);
        return $response;
    }

    public function GetCookie(Request $request){
        return $request->cookie('nameCookie');
    }

    public function PostFile(Request $request){
        // return json_encode($request->all());
        if ($request->hasFile('myFile')){
            $file = $request->file('myFile');
            $fileExtension = $file->getClientOriginalExtension('myFile');
            if (($fileExtension == 'jpg') || ($fileExtension == 'png')){
                $fileName = $file->getClientOriginalName('myFile');
                $file->move('img', $fileName);
                echo "Da luu file : ".$fileName;
            } else {
                echo "Chi duoc phep upload jpg, png";
            }
        } else {
            echo 'Chua co file';
        }
    }

    public function GetJson(){
        $array = ['truong' => 'Lavarel', 'PHP', '.Net', 'Java'];
        return response()->json($array);
    }

    public function MyView(){
        return view('test.test');
    }

    public function Time($time=""){
        return view('myView',['thoiGian'=>$time]);
    }

    public function Blade($str){
        $ten = "<b>Truong</b>";
        $arrayTest = ['Php', 'android', 'java', 'C#', 'html', 'css'];
        if ($str == 'laravel') return view('pages.laravel',['ten'=>$ten]);
        if ($str == 'php') return view('pages.php',['ten'=>$ten, 'arrayTest'=>$arrayTest]);
    }
}
