from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.post_list, name='post_list'),
    url(r'^about/', views.about, name=''),
    url(r'^contact/', views.contact, name=''),
    url(r'^search/', views.post_list_by_search, name='post_list_by_search'),
    url(r'^post/(?P<pk>\d+)/$', views.post_detail, name='post_detail'),
    url(r'^post/new/$', views.post_new, name='post_new'),
    url(r'^category/$', views.category_all, name='category_all'),
    url(r'^category/new/$', views.category_new, name='category_new'),
    url(r'^category/(?P<pk>\d+)/edit$', views.category_edit, name='category_edit'),
    url(r'^category/(?P<pk>\d+)/remove/$', views.category_remove, name='category_remove'),
    url(r'^category/(?P<pk>\d+)/publish/$', views.category_publish, name='category_publish'),
    url(r'^post/(?P<pk>\d+)/unpublish/$', views.category_unpublish, name='category_unpublish'),
    url(r'^post/(?P<pk>\d+)/edit/$', views.post_edit, name='post_edit'),
    url(r'^drafts/$', views.post_draft_list, name='post_draft_list'),
    url(r'^post/(?P<pk>\d+)/publish/$', views.post_publish, name='post_publish'),
    url(r'^post/(?P<pk>\d+)/remove/$', views.post_remove, name='post_remove'),
    url(r'^post/(?P<pk>\d+)/comment/$', views.post_detail, name='add_comment_to_post'),
    url(r'^post/author/(?P<pk>\d+)/$', views.post_list_by_author, name='post_list_by_author'),
    url(r'^comment/(?P<pk>\d+)/approve/$', views.comment_approve, name='comment_approve'),
    url(r'^comment/(?P<pk>\d+)/remove/$', views.comment_remove, name='comment_remove'),
    url(r'^comment/(?P<pk>\d+)/like/$', views.comment_like, name='comment_like'),
    url(r'^category/(?P<pk>\d+)/$', views.post_list_by_category, name='post_list_by_category'),
    # Loadmore
    url(r'^page/(?P<pk>\d+)/$', views.post_list_by_page, name='post_list_by_page'),

    #API
    url(r'^api/category/$', views.api_category_all, name='api_category_all'),
    url(r'^api/comment/recent/$', views.api_comment_recently, name='api_comment_recently'),
    url(r'^api/post/(?P<pk>\d+)/comment/$', views.api_get_comments, name='api_get_comments'),
    url(r'^api/post/$', views.api_post_all, name='api_post_all'),
    url(r'^api/post/recent$', views.api_post_recent, name='api_post_recent'),
    url(r'^api/post/(?P<pk>\d+)$', views.api_get_post, name='api_get_post'),
    #Angular2
    # url(r'^$', views.indexBlog, name='indexBlog'),
]