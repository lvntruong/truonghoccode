from django import forms
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from ckeditor.widgets import CKEditorWidget

from .models import Post, Comment, Category


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ('title', 'picture', 'text', 'category', 'author' )
        widgets = {
            'category': forms.CheckboxSelectMultiple(),
            'text': forms.CharField(widget=CKEditorUploadingWidget())
        }
        labels = {
            "picture": "URL Images"
        }

class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = ('name', 'enable_category',)

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('author', 'text',)
        labels = {
            "author": "Name",
            "text": "Comments"
        }
        widgets = {
            'text': CKEditorWidget()
        }