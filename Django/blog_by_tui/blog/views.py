from datetime import datetime
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone, formats
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponseRedirect, JsonResponse
from django.utils.text import Truncator
from django.core.urlresolvers import reverse
from .models import Post, Comment, Category
from .forms import PostForm, CommentForm, CategoryForm


def indexBlog(request):
    return render(request, 'angular2/index.html', {})

def paginator(request, obj, num_per_page):
    page = request.GET.get('page')
    paginator = Paginator(obj, num_per_page) # Show 4 obj per page
    try:
        obj = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        obj = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        obj = paginator.page(paginator.num_pages)
    return obj

def paginator_get_page(request, obj, num_per_page, page):
    paginator = Paginator(obj, num_per_page) # Show 4 obj per page
    try:
        obj = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        obj = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        obj = paginator.page(paginator.num_pages)
    return obj

def comment_like(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    comment.like += 1
    comment.save()
    # Always return an HttpResponseRedirect after successfully dealing
    # with POST data. This prevents data from being posted twice if a
    # user hits the Back button.
    # return HttpResponseRedirect(reverse('post_detail', args=(comment.post_id,)))
    response = {
        'result' : 1
    }
    return JsonResponse(response)

def about(request):
    return render(request, 'blog/about.html')

def contact(request):
    return render(request, 'blog/contact.html')

def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now(), category__enable_category=1).order_by('-published_date').distinct()
    posts = paginator(request, posts, 2)

    comments = Comment.objects.filter(approved_comment=1).order_by('-created_date')[:5]
    categories = Category.objects.filter(enable_category=1)
    return render(request, 'blog/post_list.html', {'posts': posts, 'comments': comments, 'categories' : categories})

def post_list_by_category(request, pk):
    posts = Post.objects.filter(published_date__lte=timezone.now(), category__id=pk, category__enable_category=1).order_by('-published_date').distinct()
    posts = paginator(request, posts, 4)
    return render(request, 'blog/post_list.html', {'posts': posts})

def post_list_by_author(request, pk):
    posts = Post.objects.filter(published_date__lte=timezone.now(), author_id=pk, category__enable_category=1).order_by('-published_date').distinct()
    posts = paginator(request, posts, 4)
    return render(request, 'blog/post_list.html', {'posts': posts})

def post_list_by_search(request):
    s = request.GET.get('s', '')
    posts = Post.objects.filter(published_date__lte=timezone.now(), title__icontains=s, category__enable_category=1).order_by('-published_date').distinct()
    posts = paginator(request, posts, 4)
    return render(request, 'blog/post_list.html', {'posts': posts})

# @csrf_exempt
def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = CommentForm(request.POST)
        print(form.is_valid())
        if form.is_valid():
            print("OK")
            comment = form.save(commit=False)
            comment.post = post
            comment.save()
            # return redirect('post_detail', pk=post.pk)
            response = {
                'result' : 1,
                'data' : {
                    'id' : comment.id, 
                    'datetime' : formats.date_format(timezone.localtime(comment.created_date), "SHORT_DATETIME_FORMAT"),
                }
            }
            return JsonResponse(response)
    else:
        form = CommentForm()
        return render(request, 'blog/post_detail.html', {'post': post, 'form': form})

@login_required
def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            # post.published_date = timezone.now()
            post.save()
            form.save_m2m() #important -_-
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/post_edit.html', {'form': form})

@login_required
def category_new(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('category_all')
    else:
        form = CategoryForm()
    return render(request, 'blog/category_new.html', {'form': form})

@login_required
def category_all(request):
    categories = Category.objects.all()
    return render(request, 'blog/category_all.html', {'categories' : categories})

@login_required
def category_edit(request, pk):
    category = get_object_or_404(Category, pk=pk)
    if request.method == "POST":
        form = CategoryForm(request.POST, instance=category)
        if form.is_valid():
            form.save()
            return redirect('category_all')
    else:
        form = CategoryForm(instance=category)
    return render(request, 'blog/category_edit.html', {'form': form})

@login_required
def post_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            # post.author = request.user
            # post.published_date = timezone.now()
            post.save()
            form.save_m2m() #important -_-
            return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'blog/post_edit.html', {'form': form})

@login_required
def post_draft_list(request):
    posts = Post.objects.filter(published_date__isnull=True).order_by('created_date')
    return render(request, 'blog/post_draft_list.html', {'posts': posts})

@login_required
def post_publish(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.publish()
    return redirect('post_detail', pk=pk)

@login_required
def category_publish(request, pk):
    category = get_object_or_404(Category, pk=pk)
    category.publish()
    return redirect('category_all')

@login_required
def category_unpublish(request, pk):
    category = get_object_or_404(Category, pk=pk)
    category.unpublish()
    return redirect('category_all')

@login_required
def post_remove(request, pk):
    post = get_object_or_404(Post, pk=pk)
    post.delete()
    return redirect('post_list')

@login_required
def category_remove(request, pk):
    category = get_object_or_404(Category, pk=pk)
    category.delete()
    return redirect('category_all')

@login_required
def comment_approve(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    comment.approve()
    # return redirect('post_detail', pk=comment.post.pk)
    response = {
        'result' : 1,
        'type' : 'approve'
    }
    return JsonResponse(response)

@login_required
def comment_remove(request, pk):
    comment = get_object_or_404(Comment, pk=pk)
    post_pk = comment.post.pk
    comment.delete()
    # return redirect('post_detail', pk=post_pk)
    response = {
        'result' : 1,
        'type' : 'remove'
    }
    return JsonResponse(response)

################################# API #################################
def api_category_all(request):
    list_category = []
    all_Category = Category.objects.all()

    for category in all_Category:
        list_category.append({
            'id' : category.id,
            'name' : category.name,
            'enable': category.enable_category,
        })

    response = {
        'result' : 1,
        'data' : list_category,
    }
    return JsonResponse(response)

def api_comment_recently(request):
    list_comment_recently = []
    comments = Comment.objects.filter(approved_comment=1).order_by('-created_date')[:5]

    for comment in comments:
        list_comment_recently.append({
            'id' : comment.id,
            'post_id' : comment.post_id,
            'author': comment.author,
            'post': comment.post.title,
        })

    response = {
        'result' : 1,
        'data' : list_comment_recently,
    }
    return JsonResponse(response)

def api_post_all(request):
    api_post_all = []
    posts = Post.objects.filter(published_date__lte=timezone.now(), category__enable_category=1).order_by('-published_date').distinct()
    posts = paginator(request, posts, 4)

    for post in posts:
        arrCategory = []
        for category in post.category.all():
            arrCategory.append({
                'id' : category.id,
                'name' : category.name,
                'enable' : category.enable_category,
            })
                
        api_post_all.append({
            'id' : post.id,
            'title' : post.title,
            'text' : Truncator(post.text).words(100),
            'created_date' : post.created_date,
            'picture' : post.picture,
            'published_date' : post.published_date,
            'totalComments' : api_get_total_comments(post.id),
            'author' : {
                'id' : post.author.id,
                'name' : post.author.username
            },
            'category': arrCategory
        })

    next_page_number = 0
    previous_page_number = 0
    if (posts.has_next()) is True:
        next_page_number = posts.next_page_number()
    if (posts.has_previous()) is True:
        previous_page_number = posts.previous_page_number()

    response = {
        'result' : 1,
        'data' : {
            'post' : api_post_all,
            'paginator' : {
                'has_other_pages' : posts.has_other_pages(),
                'has_next' : posts.has_next(),
                'has_previous' : posts.has_previous(),
                'number' : posts.number,
                'num_pages' : posts.paginator.num_pages,
                'page_range' : posts.paginator.num_pages+1,             
                'previous_page_number' : previous_page_number,
                'next_page_number' : next_page_number
            }
        }
    }
    return JsonResponse(response)

def api_post_recent(request):
    api_post_recent = []
    posts = Post.objects.filter(published_date__lte=timezone.now(), category__enable_category=1).order_by('-published_date').distinct()
    posts = paginator(request, posts, 5)

    for post in posts:
        api_post_recent.append({
            'id' : post.id,
            'title' : post.title,
        })

    response = {
        'result' : 1,
        'data' : api_post_recent,
    }
    return JsonResponse(response)

def api_get_comments(request, pk):
    comments = Comment.objects.filter(post_id=pk)
    api_comment = []
    for comment in comments:
        api_comment.append({
            'id' : comment.id,
            'title' : comment.author,
            'text' : comment.text,
            'created_date' : comment.created_date,
            'approved_comment' : comment.approved_comment,
            'post_id' : comment.post_id,
            'like' : comment.like
        })

    response = {
        'result' : 1,
        'data' : api_comment,
    }
    return JsonResponse(response)

def api_get_total_comments(postId):
    comments = Comment.objects.filter(post_id = postId)
    return comments.count()

def api_get_post(request, pk):
    post = get_object_or_404(Post, pk=pk)

    arrCategory = []
    for category in post.category.all():
        arrCategory.append({
            'id' : category.id,
            'name' : category.name,
            'enable' : category.enable_category,
        })

    comments = Comment.objects.filter(post_id=pk)
    api_comment = []
    for comment in comments:
        api_comment.append({
            'id' : comment.id,
            'author': comment.author,
            'text' : comment.text,
            'created_date' : comment.created_date,
            'approved_comment' : comment.approved_comment,
            'like' : comment.like
        })

    response = {
        'result' : 1,
        'data' : {
            'id' : post.id,
            'title' : post.title,
            'text' : post.text,
            'created_date' : post.created_date,
            'picture' : post.picture,
            'published_date' : post.published_date,
            'totalComments' : api_get_total_comments(post.id),
            'author' : {
                'id' : post.author.id,
                'name' : post.author.username
            },
            'category': arrCategory,
            'comment' : api_comment
        },
    }
    return JsonResponse(response)

def post_list_by_page(request, pk):
    api_post_by_page = []
    posts = Post.objects.filter(published_date__lte=timezone.now(), category__enable_category=1).order_by('-published_date').distinct()
    posts = paginator_get_page(request, posts, 2, pk) # 2 trang moi lan

    for post in posts:
        arrCategory = []
        for category in post.category.all():
            arrCategory.append({
                'id' : category.id,
                'name' : category.name,
                'enable' : category.enable_category,
            })
                
        api_post_by_page.append({
            'id' : post.id,
            'title' : post.title,
            'text' : Truncator(post.text).words(20),
            'created_date' : post.created_date,
            'picture' : post.picture,
            'published_date' : post.published_date,
            'totalComments' : api_get_total_comments(post.id),
            'author' : {
                'id' : post.author.id,
                'name' : post.author.username
            },
            'category': arrCategory
        })

    response = {
        'result' : 1,
        'data' : api_post_by_page
    }

    return JsonResponse(response)