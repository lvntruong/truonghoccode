from django.db import models
from django.utils import timezone
from ckeditor_uploader.fields import RichTextUploadingField

class Category(models.Model):
    name = models.CharField(max_length=100);
    enable_category = models.BooleanField(default=True)

    def enable(self):
        self.enable_category = True
        self.save()

    def __str__(self):
        return self.name

    def publish(self):
        self.enable_category = 1
        self.save()

    def unpublish(self):
        self.enable_category = 0
        self.save()

class Post(models.Model):
    author = models.ForeignKey('auth.User')
    category = models.ManyToManyField(Category)
    title = models.CharField(max_length=200)
    picture = models.CharField(max_length=200, default='/static/no_images.png')
    # text = models.TextField()
    text = RichTextUploadingField()
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title

    def approved_comments(self):
        return self.comments.filter(approved_comment=True)

class Comment(models.Model):
    post = models.ForeignKey('blog.Post', related_name='comments')
    author = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    like = models.IntegerField(default=0)
    approved_comment = models.BooleanField(default=False)

    def approve(self):
        self.approved_comment = True
        self.save()

    def __str__(self):
        return self.text

class ReactFacebook(models.Model):
    author = models.ForeignKey('auth.User')
    comment = models.ForeignKey(Comment)
    typeReact = models.IntegerField(default=0)

    def __str__(self):
        return self.author.username