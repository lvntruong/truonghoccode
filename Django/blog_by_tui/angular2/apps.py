from django.apps import AppConfig


class Angular2Config(AppConfig):
    name = 'angular2'
