from django.contrib import admin
from .models import Hero, Snippet

admin.site.register(Hero)
admin.site.register(Snippet)