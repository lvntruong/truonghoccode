import { Component, Input } from '@angular/core';

@Component({
  selector: 'tp-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})

export class ContactListComponent {
  message: string = "Hi, I'm from ContactListComponent";
  doClick(){
    this.message = "Button Clicked";
    setTimeout(() => {
      this.message = "Button Done";
    }, 2000);
  }
  printable = true;
  @Input() fromParentComponent: string;
  
}