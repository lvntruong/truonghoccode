import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tp-contact-image-detail',
  templateUrl: './contact-image-detail.component.html',
  styleUrls: ['./contact-image-detail.component.css']
})
export class ContactImageDetailComponent implements OnInit {
  avatar: any = {};
  constructor() { }

  ngOnInit() {
    this.avatar.url = "http://www.tiepphan.com/wp-content/uploads/2016/12/component-hierarchy.png";
  }

}
