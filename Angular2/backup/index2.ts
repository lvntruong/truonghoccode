import { Component, OnInit, OnDestroy, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { CoinCapService } from '../services/coin-cap.service';
import { PostService } from '../services/post.service';


declare var $:any;
// declare var CCC:any;

@Component({
  selector: 'index-page',
  templateUrl: './index.html',
  styleUrls: ['./style.css'],
  providers: [CoinCapService]
})
export class IndexComponent implements OnInit, OnDestroy {
  private connection;
  toogle;
  tables : any[] = [];
  private array : any[] = [];
  private currencyRates = {};
  private data : any;

  private coins = {
      "USD": {
          "name": "Dollar",
          "symbol": "USD",
          "image_url": "assets/images/coins/dollar.png",
          "get_usd_price": function(){return 1;}
      }
  };

	constructor(private coinCapService:CoinCapService, private _postService:PostService) {
  }

  ngOnInit() {
    this._postService.getSubscriptionURL('http://coincap.io/exchange_rates').subscribe(res => {
      this.currencyRates = res;
      this._postService.getSubscriptionURL('http://coincap.io/front').subscribe(res => {
        this.build_coins(res);
      });
    });
  }
  
  ngOnDestroy() {
    // this.connection.unsubscribe();
  }

  openSocket(){
    this.connection = this.coinCapService.getMessages().subscribe(data => {
      this.data = data;
      console.log(data);
      let coin = this.data.message.msg.short;
      console.log(coin);
    })
  }

  build_coins(cc_coins){
    // console.log(cc_coins);
    let coins = {
      "USD": {
          "name": "Dollar",
          "symbol": "USD",
          "image_url": "assets/images/coins/dollar.png",
          "get_usd_price": function(){return 1;}
      }
    };

    let this_cha = this;
    let on_load = function(any){};

    (function load_tradeable_coins(){
      let url = "https://shapeshift.io/getcoins";
      this_cha._postService.getSubscriptionURL(url).subscribe(res => {
        try{
          let ss_coins = res;
            for(let coin_symbol in ss_coins){
              if(ss_coins[coin_symbol].status === "available" && coins[coin_symbol]){
                coins[coin_symbol].tradeable = true;
                coins[coin_symbol].cell.trade.content.style = "visibility: visible";
              }
            on_load && on_load(coins);
            }
        }
        catch(e){
          // on_load && on_load(coins);
          // on_load = function(){};
        }

        setTimeout(load_tradeable_coins, 5000);
        console.log(coins);
        console.log(on_load);
      });
    })();

  }
}
