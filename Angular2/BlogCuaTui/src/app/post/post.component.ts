import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';

import { Post } from '../bean/post'
import { Paginator } from './paginator'
import { PostService } from '../services/post.service';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';

@Component({
  selector: 'lvnt-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  idPage = 1;
  allPosts: Post[] = [];
  paginator: Paginator = new Paginator();
  totalComments = 0;
  arrNumber : any[] = [];

  constructor(private postService: PostService,
              private route: ActivatedRoute,
              private location: Location
  ) { }

  ngOnInit() {
    // this.route.params
    //   .switchMap((params: Params) => this.postService.getAllPost(+params['id']))
    //   .subscribe(dataFromServer => {
    //                                   this.allPosts = dataFromServer.post;
    //                                   this.paginator = dataFromServer.paginator;
    //                                   for (var i = 1; i < dataFromServer.paginator.page_range; i++) {
    //                                     this.arrNumber[i-1] = i;
    //                                   }
    //                                 }
    //             );

    // this.postService.getAllPost()
    //     .then(dataFromServer => {
    //         this.allPosts = dataFromServer.post;
    //         this.paginator = dataFromServer.paginator;
    //         for (var i = 1; i < dataFromServer.paginator.page_range; i++) {
    //           this.arrNumber[i-1] = i;
    //       }
    // })

    this.route.params.subscribe(params => {
       // In a real app: dispatch action to load the details here.
       this.postService.getAllPost(+params['id']) // (+) converts string 'id' to a number
        .then(dataFromServer => {
            this.allPosts = dataFromServer.post;
            this.paginator = dataFromServer.paginator;
            for (var i = 1; i < dataFromServer.paginator.page_range; i++) {
              this.arrNumber[i-1] = i;
          }
      })
    });
    
  }

  goBack(): void {
    this.location.back();
  }
}