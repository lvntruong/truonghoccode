export class Paginator {
    has_other_pages : boolean;
    has_next : boolean;
    has_previous : boolean;
    number : number;
    num_pages : number;
    page_range : number;     
    previous_page_number : number;
    next_page_number : number;
}
