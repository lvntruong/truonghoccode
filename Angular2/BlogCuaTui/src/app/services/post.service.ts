import { Component, OnInit, Inject, Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { DOCUMENT } from '@angular/platform-browser';
import 'rxjs/add/operator/toPromise';

import { DataPost } from '../bean/dataPost'
import { Post } from '../bean/post'

@Injectable()
export class PostService implements OnInit {
  private url : String;
  private postListUrl = '/api/post/?page=';  // URL to web api
  private postUrl = '/api/post/';  // URL to web api
  private postRecentUrl = '/api/post/recent';  // URL to web api

  constructor(private http: Http, @Inject(DOCUMENT) private document: any) {
    this.url = document.location.protocol +'//'+ document.location.hostname + ':8000';
    // console.log(this.url);
  }

  getAllPost(id:number): Promise<DataPost> {
    // console.log(id +"-"+ isNaN(id));
    // if (isNaN(id)) id = 1;
    return this.http.get(this.url + this.postListUrl+id)
               .toPromise()
               .then(response => response.json().data as DataPost)
               .catch(this.handleError);
  }

  getPost(id:number): Promise<Post> {
    // console.log(id +"-"+ isNaN(id));
    // if (isNaN(id)) id = 1;
    return this.http.get(this.url + this.postUrl+id)
               .toPromise()
               .then(response => response.json().data as Post)
               .catch(this.handleError);
  }

  getRecentPost(): Promise<Post[]> {
    return this.http.get(this.url + this.postRecentUrl)
               .toPromise()
               .then(response => response.json().data as Post[])
               .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

  ngOnInit() {
    
  }

}
