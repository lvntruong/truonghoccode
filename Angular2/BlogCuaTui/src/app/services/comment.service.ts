import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { Comment } from '../bean/comment'

@Injectable()
export class CommentService {

  private recentCommentUrl = 'http://localhost:8000/api/comment/recent/';

  constructor(private http: Http) { }

  getRecentComment(): Promise<Comment[]> {
    return this.http.get(this.recentCommentUrl)
               .toPromise()
               .then(response => response.json().data as Comment[])
               .catch(this.handleError);
  }

  getTotalComments(num): Promise<number> {
    return this.http.get('http://localhost:8000/api/post/'+num+'/comment/total')
               .toPromise()
               .then(response => response.json().data as number)
               .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}