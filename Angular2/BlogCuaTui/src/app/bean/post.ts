export class Post {
  id: number;
  title: string;
  text: string;
  created_date: string;
  published_date: string;
  author: any;
  picture: string;
  totalComments: number;
  category: any[];
  comment: any;
}