export class Comment {
  id: number;
  author: string;
  text: string;
  created_date: string;
  approved_comment: boolean;
  post_id: number;
  like: number;
}
