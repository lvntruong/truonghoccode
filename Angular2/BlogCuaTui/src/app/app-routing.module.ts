import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AboutComponent }       from './about/about.component';
import { ContactComponent }     from './contact/contact.component';
import { PostComponent }        from './post/post.component';
import { PostDetailComponent }  from './post-detail/post-detail.component';
import { LoginComponent }       from './login/login.component';

const routes: Routes = [
  { path: '',  component: PostComponent },
  { path: 'about',  component: AboutComponent },
  { path: 'contact',  component: ContactComponent },
  { path: 'page/:id', component: PostComponent },
  { path: 'post/:id', component: PostDetailComponent },
  { path: 'accounts/login', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
