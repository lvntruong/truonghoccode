import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AboutComponent } from './about/about.component';

import { CategoryService } from './services/category.service';
import { CommentService } from './services/comment.service';
import { PostService } from './services/post.service';
import { ContactComponent } from './contact/contact.component';
import { PostComponent } from './post/post.component';

import { TruncateModule } from 'ng2-truncate';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    AboutComponent,
    ContactComponent,
    PostComponent,
    PostDetailComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    TruncateModule
  ],
  providers: [CategoryService, CommentService, PostService],
  bootstrap: [AppComponent]
})
export class AppModule { }
