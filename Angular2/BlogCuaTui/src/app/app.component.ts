import { Component, OnInit } from '@angular/core';

import { Category } from './bean/category'
import { CategoryService } from './services/category.service';
import { Comment } from './bean/comment'
import { CommentService } from './services/comment.service';
import { Post } from './bean/post'
import { PostService } from './services/post.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  categories: Category[] = [];
  recentComments: Comment[] = [];
  allPosts: Post[] = [];

  constructor(private categoryService: CategoryService, 
              private commentService: CommentService,
              private postService: PostService) { }

  ngOnInit(): void {
    this.categoryService.getCategories()
      .then(dataFromServer => this.categories = dataFromServer);
      // .then(dataFromServer => console.log(dataFromServer));

    this.commentService.getRecentComment()
      .then(dataFromServer => this.recentComments = dataFromServer);
      // .then(dataFromServer => console.log(dataFromServer));

    this.postService.getRecentPost()
      .then(dataFromServer => this.allPosts = dataFromServer);
      // .then(dataFromServer => console.log(dataFromServer));
    
  }
}
