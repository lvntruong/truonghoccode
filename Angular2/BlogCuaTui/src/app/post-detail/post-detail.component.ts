import 'rxjs/add/operator/switchMap';
import { Component, OnInit } from '@angular/core';

import { Post } from '../bean/post'
import { PostService } from '../services/post.service';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';

@Component({
  selector: 'lvnt-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {
  post : Post = new Post();
  constructor(private postService: PostService,
              private route: ActivatedRoute,
              private location: Location
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
       // In a real app: dispatch action to load the details here.
       this.postService.getPost(+params['id']) // (+) converts string 'id' to a number
        .then(dataFromServer => {
            this.post = dataFromServer;
      })
    });
  }

  goBack(): void {
    this.location.back();
  }

}
