import { BlogCuaTuiPage } from './app.po';

describe('blog-cua-tui App', () => {
  let page: BlogCuaTuiPage;

  beforeEach(() => {
    page = new BlogCuaTuiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
