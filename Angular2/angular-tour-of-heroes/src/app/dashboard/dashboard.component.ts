import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';

@Component({
  selector: 'lvnt-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  heroes: Hero[] = [];
  constructor(private heroService: HeroService) { }
  ngOnInit(): void {
  this.heroService.getHeroes()
    .then(dataFromServer => this.heroes = dataFromServer.slice(1, 5));
    // .then(dataFromServer => console.log(dataFromServer));
  }

}
