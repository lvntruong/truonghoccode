<?php 
  $params = '';
?>
<div class="row row-offcanvas row-offcanvas-right">
  <div class="col-xs-12 col-sm-12">
<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo html_helpers::url(
		array('ctl'=>'pagination', 
			  'act'=>$this->action, 
			  'params'=>$params
)); ?>">

  <div class="form-group">
    <label for="products" class="col-sm-2 control-label">Set: </label>
    <div class="col-sm-10">
      <input name="data[<?php echo $this->controller; ?>][products]" type="text" class="form-control" id="products" placeholder="number item view" <?php echo (isset($this->record))? "value='".$this->record['products']."'":""; ?>>
    </div>
  </div>  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button name="btn_submit" type="submit" class="btn btn-default submit"><?php echo ucwords($this->action); ?></button>
    </div>
  </div>
</form>
<script src="media/js/form.js"></script>
</div> 
</div>