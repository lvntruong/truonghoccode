<div class="list-group">
	<a href="#" class="list-group-item active">
		<h4 class="list-group-item-heading">Management products</h4>
	</a>
	<a href="<?php echo html_helpers::url(array('ctl'=>'products')); ?>" class="list-group-item">List all products</a>
	<a href="#" class="list-group-item add" data-toggle="modal" data-target="#myModal">Add new products</a>
	<a href="#" class="list-group-item no_pagination" data-toggle="modal" data-target="#myModal">Set pagination</a>
</div>