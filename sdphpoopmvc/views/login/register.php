<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
  <title>Sign-Up/Login Form</title>
  <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel="stylesheet" href="views/login/css/style.css">
</head>

<body>

<div class="form">
      
      <ul class="tab-group">
        <li class="tab"><a href="<?php echo html_helpers::url(array('ctl'=>'login', 'act'=>'login', 'params'=>''
)); ?>">Log In</a></li>
        <li class="tab active"><a href="#">Sign Up</a></li>
      </ul>
      
      <div class="tab-content">


        <div id="signup">   
          <h1>Sign Up for Free</h1>          
          <form action="<?php echo html_helpers::url(array('ctl'=>'login', 'act'=>'register', 'params'=>''
)); ?>" method="post">          
          <div class="top-row">
            <div class="field-wrap">
              <label>
                First Name<span class="req">*</span>
              </label>
              <input type="text" name="data[<?php echo $this->controller; ?>][firstname]" required autocomplete="off" />
            </div>
        
            <div class="field-wrap">
              <label>
                Last Name<span class="req">*</span>
              </label>
              <input type="text" name="data[<?php echo $this->controller; ?>][lastname]" required autocomplete="off"/>
            </div>
          </div>

          <div class="field-wrap">
            <label>
              Username<span class="req">*</span>
            </label>
            <input type="text" name="data[<?php echo $this->controller; ?>][username]" required autocomplete="off"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Set A Password<span class="req">*</span>
            </label>
            <input type="password" name="data[<?php echo $this->controller; ?>][password]" required autocomplete="off"/>
          </div>
          
          <button type="submit" name="btn_submit" class="button button-block"/>Get Started</button>
          
          </form>

        </div>

        <div id="login">   
          
        </div>
        
      </div><!-- tab-content -->
</div> <!-- /form -->

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="views/login/js/index.js"></script>

</body>
</html>
