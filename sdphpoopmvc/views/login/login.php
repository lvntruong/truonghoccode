<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Sign-Up/Login Form</title>
  <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel="stylesheet" href="views/login/css/style.css">
  <?php
    if (isset($_GET["error"])) {
      $error = $_GET["error"];
      if ($error ==1) {
      echo "<script>alert('Invalid Username or Password')</script>";
      }
    }
  ?>
</head>

<body>

<div class="form">
      <ul class="tab-group">
        <li class="tab active"><a href="#">Log In</a></li>
        <li class="tab"><a href="<?php echo html_helpers::url(array('ctl'=>'login', 'act'=>'register', 'params'=>''
)); ?>">Sign Up</a></li>
      </ul>

      <div class="tab-content">

        <div id="login">   
          <h1>Welcome Back!</h1>
          
          <form action="<?php echo html_helpers::url(array('ctl'=>'login', 'act'=>'login', 'params'=>''
)); ?>" method="post">
          
            <div class="field-wrap">
            <label>
              Username<span class="req">*</span>
            </label>
            <input type="text" name="data[<?php echo $this->controller; ?>][username]" required autocomplete="off"/>
          </div>
          
          <div class="field-wrap">
            <label>
              Password<span class="req">*</span>
            </label>
            <input type="password" name="data[<?php echo $this->controller; ?>][password]" required autocomplete="off"/>
          </div>
          
          <p class="forgot"><a href="#">Forgot Password?</a></p>
          
          <button name="btn_submit" class="button button-block"/>Log In</button>
          
          </form>
        </div>

        <div id="signup">   

        </div>
        
      </div><!-- tab-content -->
</div> <!-- /form -->

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="views/login/js/index.js"></script>

</body>
</html>
