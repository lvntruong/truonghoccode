<?php 
  $params = (isset($this->record))? array('id'=>$this->record['id']):'';
?>
<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo html_helpers::url(
		array('ctl'=>'products', 
			  'act'=>$this->action, 
			  'params'=>$params
)); ?>">
  <div class="form-group">
    <label for="name" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-10">
      <input name="data[<?php echo $this->controller; ?>][name]" type="text" class="form-control" id="name" placeholder="name product" <?php echo (isset($this->record))? "value='".$this->record['name']."'":""; ?>>
    </div>
  </div>
  <div class="form-group">
    <label for="details" class="col-sm-2 control-label">Details</label>
    <div class="col-sm-10">
      <input name="data[<?php echo $this->controller; ?>][details]" type="text" class="form-control" id="details" placeholder="details" <?php echo (isset($this->record))? "value='".$this->record['details']."'":""; ?>>
    </div>
  </div>
  <div class="form-group">
    <label for="introduce" class="col-sm-2 control-label">Introduce</label>
    <div class="col-sm-10">
      <input name="data[<?php echo $this->controller; ?>][introduce]" type="text" class="form-control" id="introduce" placeholder="introduce" <?php echo (isset($this->record))? "value='".$this->record['introduce']."'":""; ?>>
    </div>
  </div>
  <div class="form-group">
    <label for="id_category" class="col-sm-2 control-label">Brand</label>
    <div class="col-sm-10">
<!--       <input name="data[<?php echo $this->controller; ?>][id_category]" type="text" class="form-control" id="id_category" placeholder="id_category" <?php echo (isset($this->record))? "value='".$this->record['id_category']."'":""; ?>> -->
       <select name="data[<?php echo $this->controller; ?>][id_category]" id="id_category" class="form-control">
        <?php
          $categories = new category_model();
          $all_cat = $categories->getAllRecords();
          $select = "";
          while ($row = mysqli_fetch_array($all_cat)) {
            if ($row['id'] == $this->record['id_category']) $select = "selected=selected"; else $select = "";
        ?>
          <option value="<?php echo $row['id'] ?>" <?php echo $select ?> ><?php echo $row['name_category'] ?></option> 
        <?php
          }
        ?>
      </select>
    </div>
  </div>
  <div class="form-group">
    <label for="photo" class="col-sm-2 control-label">Photo</label>
    <div class="col-sm-10 image-upload">
      <input name="image" type="file" class="form-control" id="photo" placeholder="photo">
	  <?php if (isset($this->record)): ?>  
		<img src="<?php echo "media/upload/" .$this->controller.'/'.$this->record['photo']; ?>" alt="<?php echo $this->record['name']; ?>" class="img-thumbnail">
	  <?php endif; ?>
    </div>
  </div>
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button name="btn_submit" type="submit" class="btn btn-default submit"><?php echo ucwords($this->action); ?></button>
    </div>
  </div>
</form>
<script src="media/js/form.js"></script>