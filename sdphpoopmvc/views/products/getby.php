<?php include_once 'views/layout/'.$this->layout.'header.php'; ?>
<div class="row row-offcanvas row-offcanvas-right">
  <div class="col-xs-12 col-sm-9">
	<div class="table-responsive">
	  <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Details</th>
            <th>Introduce</th>
            <th>Brand</th>
            <th>Photo</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
		<?php while($row = mysqli_fetch_array($this->records)) : ?>
		  <tr>
			<th width="5%" scope="row"><?php echo $row['id']; ?></th>
			<td width="12%"><?php echo $row['name']; ?></td>
			<td width="30%"><?php echo $row['details']; ?></td>
			<td width="12%"><?php echo $row['introduce']; ?></td>
			<td width="11%"><?php echo $row['name_category']; ?></td>
			<td width="15%"><img src="<?php echo "media/upload/" .$this->controller.'/'.$row['photo']; ?>" alt="<?php echo $row['name']; ?>" class="img-thumbnail"></td>
			<td width="15%">
			  <a href="#" class="table-link view" data-toggle="modal" data-target="#myModal">
				<span class="fa-stack">
				<i class="fa fa-square fa-stack-2x"></i>
				<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
				</span>
			  </a>
			  <a href="#" class="table-link edit" data-toggle="modal" data-target="#myModal">
				<span class="fa-stack">
				<i class="fa fa-square fa-stack-2x"></i>
				<i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
				</span>
			  </a>
			  <a href="#" class="table-link danger delete" data-toggle="modal" data-target="#myModal">
				<span class="fa-stack">
				<i class="fa fa-square fa-stack-2x"></i>
				<i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
				</span>
			  </a>
			</td>
		  </tr>
		<?php endwhile; ?>
        </tbody>
      </table>
	</div>

	<div style="float: right;">
		<ul class="pagination">
		    <li class="active"><a href="#">1</a></li>
		    <li><a href="#">2</a></li>
		    <li><a href="#">3</a></li>
		    <li><a href="#">4</a></li>
		    <li><a href="#">5</a></li>
	  </ul>
	</div>
  </div>

<!-- Popup View -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">VIEW</h4>
      </div>
      <div class="modal-body">
        <p>PLEASE WAIT...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- End Popup View -->


  <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
	<?php include_once 'views/widgets/sidebar_products.php'; ?>
  </div>
</div>

<script>
	$( document ).ready(function() {
		function bindButtonClick(){
			$modal_footer_backup = $(".modal-footer").html();
			$(".view").on("click", function(){
				// alert($(this).parents("tr").find("th").html());
				$id = $(this).parents("tr").find("th").html();
			    $.ajax({
			    	url: "<?php echo html_helpers::url(array('ctl'=>'products', 'act'=>'view', 'params'=>array('id'=>''))); ?>" + $id,
					success: function(result){
						$(".modal-title").html("VIEW");
				        $(".modal-body").html(result);
				        $(".modal-footer").html($modal_footer_backup);
			    	}
				});
			});
			$(".edit").on("click", function(){
				$id = $(this).parents("tr").find("th").html();
			    $.ajax({
			    	url: "<?php echo html_helpers::url(array('ctl'=>'products', 'act'=>'edit', 'params'=>array('id'=>''))); ?>" + $id,
					success: function(result){
						$(".modal-title").html("EDIT");
				        $(".modal-body").html(result);
				        $(".modal-footer").html($modal_footer_backup);
				        bindButtonClick();
			    	}
				});
			});
			$(".delete").on("click", function(){
				$id = $(this).parents("tr").find("th").html();
				$(".modal-title").html("DELETE");
				$(".modal-body").html("Are you sure you wish to delete?");
			    $(".modal-footer").html('<button class="btn btn-default btn-danger delete-yes" tabindex="0">Delete</button>');
				$(".delete-yes").click(function(){
					$.ajax({
				    	url: "<?php echo html_helpers::url(array('ctl'=>'products', 'act'=>'del', 'params'=>array('id'=>''))); ?>" + $id,
						success: function(result){
							refresh();
							$(".close").click();
							$(".modal-footer").html($modal_footer_backup);
				    	}
					});
				});
			});
			$(".add").on("click", function(){
			    $.ajax({
			    	url: "<?php echo html_helpers::url(array('ctl'=>'products', 'act'=>'add')); ?>",
					success: function(result){
						$(".modal-title").html("ADD");
				        $(".modal-body").html(result);
				        bindButtonClick();
			    	}
				});
			});
			$("button.submit").on("click", function(){
			    $(".form-horizontal").ajaxForm({ 
			        // target: '#htmlExampleTarget', 
			        success: function() { 
			            refresh();
			            $(".close").click();
			        } 
			    }); 
			});
		}
		bindButtonClick();

		function refresh(){
			$.ajax({
		    	url: "<?php echo html_helpers::url(array('ctl'=>'products', 'act'=>'refresh')); ?>",
				success: function(result){
					$("tbody").html(result);
					bindButtonClick();
		    	}
			});
		}
	});
</script>

<?php include_once 'views/layout/'.$this->layout.'footer.php'; ?>