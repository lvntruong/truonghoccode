<?php include_once 'views/layout/'.$this->layout.'header.php'; ?>
<div class="row row-offcanvas row-offcanvas-right">
  <div class="col-xs-12 col-sm-9">
	<div class="table-responsive">
	  <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Details</th>
            <th>Introduce</th>
            <th>Brand</th>
            <th>Photo</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
		<?php while($row = mysqli_fetch_array($this->records)) : ?>
		  <tr>
			<th width="5%" scope="row"><?php echo $row['id']; ?></th>
			<td width="12%"><?php echo $row['name']; ?></td>
			<td width="30%"><?php echo $row['details']; ?></td>
			<td width="12%"><?php echo $row['introduce']; ?></td>
			<td width="11%"><?php echo $row['name_category']; ?></td>
			<td width="15%"><img src="<?php echo "media/upload/" .$this->controller.'/'.$row['photo']; ?>" alt="<?php echo $row['name']; ?>" class="img-thumbnail"></td>
			<td width="15%">
			  <a href="#" class="table-link view" data-toggle="modal" data-target="#myModal">
				<span class="fa-stack">
				<i class="fa fa-square fa-stack-2x"></i>
				<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
				</span>
			  </a>
			  <a href="#" class="table-link edit" data-toggle="modal" data-target="#myModal">
				<span class="fa-stack">
				<i class="fa fa-square fa-stack-2x"></i>
				<i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
				</span>
			  </a>
			  <a href="#" class="table-link danger delete" data-toggle="modal" data-target="#myModal">
				<span class="fa-stack">
				<i class="fa fa-square fa-stack-2x"></i>
				<i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
				</span>
			  </a>
			</td>
		  </tr>
		<?php endwhile; ?>
        </tbody>
      </table>
	</div>

  </div>

<!-- Popup View -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">VIEW</h4>
      </div>
      <div class="modal-body">
        <p>PLEASE WAIT...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- End Popup View -->


  <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
	<?php include_once 'views/widgets/sidebar_products.php'; ?>
  </div>
</div>

<script>
	$( document ).ready(function() {
		$modal_footer_backup = $(".modal-footer").html();

		$(document).on("click", ".view", function(){
			// alert($(this).parents("tr").find("th").html());
			$id = $(this).parents("tr").find("th").html();
			$.getJSON( "<?php echo html_helpers::url(array('ctl'=>'products', 'act'=>'view', 'params'=>array('id'=>''))); ?>" + $id, function( data ) {
			  var items = [];
			  $.each( data, function( key, val ) {
			    items.push( '<tr><th scope="row">'+key+'</th><td>'+val+'</td></tr>' );
			});
			  $truoc = '<div class="row row-offcanvas row-offcanvas-right"><div class="col-xs-12 col-sm-12"><div class="table-responsive"><table class="table table-bordered table-striped"><colgroup><col class="col-xs-1"><col class="col-xs-7"></colgroup><thead><tr><th>Field</th><th>Value</th></tr></thead><tbody>';
			  $sau = '</tbody></table></div></div></div>';
			  $(".modal-title").html("VIEW");
			  $(".modal-body").html($truoc + items.join( "" ) + $sau);
			  $(".modal-footer").html($modal_footer_backup);
			});
		});

		$(document).on("click", ".edit", function(){
			$id = $(this).parents("tr").find("th").html();
			$tr_cur = $(this).parents("tr");
		    $.ajax({
		    	url: "<?php echo html_helpers::url(array('ctl'=>'products', 'act'=>'edit', 'params'=>array('id'=>''))); ?>" + $id,
				success: function(result){
					$(".modal-title").html("EDIT");
			        $(".modal-body").html(result);
			        $(".modal-footer").html($modal_footer_backup);
			        callAgain($tr_cur);
		    	}
			});
		});

		$(document).on("click", ".delete", function(){
			$tr_parents = $(this).parents("tr");
			$id = $(this).parents("tr").find("th").html();
			$(".modal-title").html("DELETE");
			$(".modal-body").html("Are you sure you wish to delete?");
		    $(".modal-footer").html('<button class="btn btn-default btn-danger delete-yes" tabindex="0">Delete</button>');
			$(".delete-yes").click(function(){
				$.ajax({
			    	url: "<?php echo html_helpers::url(array('ctl'=>'products', 'act'=>'del', 'params'=>array('id'=>''))); ?>" + $id,
					success: function(result){
						$tr_parents.remove();
						$(".close").click();
						$(".modal-footer").html($modal_footer_backup);
			    	}
				});
			});
		});

		$(document).on("click", ".add", function(){
		    $.ajax({
		    	url: "<?php echo html_helpers::url(array('ctl'=>'products', 'act'=>'add')); ?>",
				success: function(result){
					$(".modal-title").html("ADD");
			        $(".modal-body").html(result);
			        $(".modal-footer").html($modal_footer_backup);
			        callAgain(null);
		    	}
			});
		});

		function callAgain($tr_cur = null){
			// $(document).on("click", "button.submit", function(){
			$("button.submit").on("click", function(){
				$tr_sample = $("tbody tr:first").html();
			    $(".form-horizontal").ajaxForm({  
			    	dataType: "json",
			        success: function(data) { 
			        	// alert(data);
			        	$name = $('#name').fieldValue();
			        	$details = $('#details').fieldValue();
			        	$introduce = $('#introduce').fieldValue();
			        	$id_category = $('#id_category option:selected').text();
			        	$obj = $.parseJSON(JSON.stringify(data));
			        	$id = $obj.id;
			        	$photo = $obj.photo;
			        	$tr_last = $tr_cur;
			        	if ($id != null){
			        		$("tbody").append('<tr>'+$tr_sample+'</tr>');
			        		$tr_last = $("tbody tr:last");
			        		$tr_last.find("th").html($id);
			        	}
			        	$tr_last.find("td").eq(0).html($name);
			        	$tr_last.find("td").eq(1).html($details);
			        	$tr_last.find("td").eq(2).html($introduce);
			        	$tr_last.find("td").eq(3).html($id_category);
			        	$tr_last.find("td").eq(4).html('<img src="media/upload/products/'+$photo+'" alt="'+$name+'" class="img-thumbnail">');
			            $(".close").click();
			        } 
			    }); 
			});
		}
	});
</script>

<?php include_once 'views/layout/'.$this->layout.'footer.php'; ?>