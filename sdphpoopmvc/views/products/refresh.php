<?php while($row = mysqli_fetch_array($this->records)) : ?>
  <tr>
	<th width="5%" scope="row"><?php echo $row['id']; ?></th>
	<td width="12%"><?php echo $row['name']; ?></td>
	<td width="30%"><?php echo $row['details']; ?></td>
	<td width="12%"><?php echo $row['introduce']; ?></td>
	<td width="11%"><?php echo $row['name_category']; ?></td>
	<td width="15%"><img src="<?php echo "media/upload/" .$this->controller.'/'.$row['photo']; ?>" alt="<?php echo $row['name']; ?>" class="img-thumbnail"></td>
	<td width="15%">
	  <a href="#" class="table-link view" data-toggle="modal" data-target="#myModal">
		<span class="fa-stack">
		<i class="fa fa-square fa-stack-2x"></i>
		<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
		</span>
	  </a>
	  <a href="#" class="table-link edit" data-toggle="modal" data-target="#myModal">
		<span class="fa-stack">
		<i class="fa fa-square fa-stack-2x"></i>
		<i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
		</span>
	  </a>
	  <a href="#" class="table-link danger delete" data-toggle="modal" data-target="#myModal">
		<span class="fa-stack">
		<i class="fa fa-square fa-stack-2x"></i>
		<i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
		</span>
	  </a>
	</td>
  </tr>
<?php endwhile; ?>