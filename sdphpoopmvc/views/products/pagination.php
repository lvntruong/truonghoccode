<?php 
  $params = (isset($this->record))? array('id'=>$this->record['id']):'';
?>
<div class="row row-offcanvas row-offcanvas-right">
  <div class="col-xs-12 col-sm-12">
<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo html_helpers::url(
		array('ctl'=>'products', 
			  'act'=>$this->action, 
			  'params'=>$params
)); ?>">
  <div class="form-group">
    <label for="name" class="col-sm-2 control-label">Name</label>
    <div class="col-sm-10">
      <input name="data[<?php echo $this->controller; ?>][name]" type="text" class="form-control" id="name" placeholder="name product" <?php echo (isset($this->record))? "value='".$this->record['name']."'":""; ?>>
    </div>
  </div>  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button name="btn_submit" type="submit" class="btn btn-default submit"><?php echo ucwords($this->action); ?></button>
    </div>
  </div>
</form>
<script src="media/js/form.js"></script>
</div> 
</div>