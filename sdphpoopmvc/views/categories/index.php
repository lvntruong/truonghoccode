<?php include_once 'views/layout/'.$this->layout.'header.php'; ?>
<div class="row row-offcanvas row-offcanvas-right">
  <div class="col-xs-12 col-sm-9">
	<div class="table-responsive">
	  <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Brand</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
		<?php while($row = mysqli_fetch_array($this->records)) : ?>
		  <tr style="cursor:pointer;">
			<th width="10%"><?php echo $row['id']; ?></th>
			<th width="75%"><?php echo $row['name_category']; ?></th>
			<td width="15%">
			  <a href="#" class="table-link view" data-toggle="modal" data-target="#myModal">
				<span class="fa-stack">
				<i class="fa fa-square fa-stack-2x"></i>
				<i class="fa fa-search-plus fa-stack-1x fa-inverse"></i>
				</span>
			  </a>
			  <a href="#" class="table-link edit" data-toggle="modal" data-target="#myModal">
				<span class="fa-stack">
				<i class="fa fa-square fa-stack-2x"></i>
				<i class="fa fa-pencil fa-stack-1x fa-inverse"></i>
				</span>
			  </a>
			  <a href="#" class="table-link danger delete" data-toggle="modal" data-target="#myModal">
				<span class="fa-stack">
				<i class="fa fa-square fa-stack-2x"></i>
				<i class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
				</span>
			  </a>
			</td>
		  </tr>
		<?php endwhile; ?>
        </tbody>
      </table>
	</div>
  </div>

<!-- Popup View -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">VIEW</h4>
      </div>
      <div class="modal-body">
        <p>PLEASE WAIT...</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- End Popup View -->

  <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
	<?php include_once 'views/widgets/sidebar_categories.php'; ?>
  </div>
</div>

<script>
	$(document).ready(function() {
		$("tbody tr th").click(function(){
			$id = $(this).parents("tr").find("th").html();
			// $.ajax({
		 //    	url: "<?php echo html_helpers::url(array('ctl'=>'products', 'act'=>'getby', 'params'=>array('id'=>''))); ?>" + $id,
			// 	success: function(result){
			// 		$(".modal-body").html(result);
			//   		// bindButtonClick();
		 //    	}
			// });
			url = "<?php echo html_helpers::url(array('ctl'=>'products', 'act'=>'getby', 'params'=>array('id'=>''))); ?>" + $id;
			$(location).attr("href", url);
		});

		$("tbody tr th").hover(function(){
		    $(this).css("background-color", "yellow");
		    }, function(){
		    $(this).css("background-color", "white");
		});

		function bindButtonClick(){
			$modal_footer_backup = $(".modal-footer").html();
			$(document).on("click",".view", function(){
				// alert($(this).parents("tr").find("th").html());
				$id = $(this).parents("tr").find("th").html();
			    $.ajax({
			    	url: "<?php echo html_helpers::url(array('ctl'=>'categories', 'act'=>'view', 'params'=>array('id'=>''))); ?>" + $id,
					success: function(result){
						$(".modal-title").html("VIEW");
				        $(".modal-body").html(result);
				        $(".modal-footer").html($modal_footer_backup);
			    	}
				});
			});
			$(document).on("click",".edit", function(){
				$id = $(this).parents("tr").find("th").html();
			    $.ajax({
			    	url: "<?php echo html_helpers::url(array('ctl'=>'categories', 'act'=>'edit', 'params'=>array('id'=>''))); ?>" + $id,
					success: function(result){
						$(".modal-title").html("EDIT");
				        $(".modal-body").html(result);
				        $(".modal-footer").html($modal_footer_backup);
				        bindButtonClick();
			    	}
				});
			});
			$(document).on("click",".delete", function(){
				$id = $(this).parents("tr").find("th").html();
				$(".modal-title").html("DELETE");
				$(".modal-body").html("Are you sure you wish to delete?");
			    $(".modal-footer").html('<button class="btn btn-default btn-danger delete-yes" tabindex="0">Delete</button>');
				$(".delete-yes").click(function(){
					$.ajax({
				    	url: "<?php echo html_helpers::url(array('ctl'=>'categories', 'act'=>'del', 'params'=>array('id'=>''))); ?>" + $id,
						success: function(result){
							refresh();
							$(".close").click();
							$(".modal-footer").html($modal_footer_backup);
				    	}
					});
				});
			});
			$(document).on("click",".add", function(){
			    $.ajax({
			    	url: "<?php echo html_helpers::url(array('ctl'=>'categories', 'act'=>'add')); ?>",
					success: function(result){
						$(".modal-title").html("ADD");
				        $(".modal-body").html(result);
				        bindButtonClick();
			    	}
				});
			});
			$("button.submit").on("click", function(){
			    $(".form-horizontal").ajaxForm({ 
			        // target: '#htmlExampleTarget', 
			        success: function() { 
		            $(".close").click();
		            refresh();
			        } 
			    }); 
			});
		}
		bindButtonClick();

		function refresh(){
			$.ajax({
		    	url: "<?php echo html_helpers::url(array('ctl'=>'categories', 'act'=>'refresh')); ?>",
				success: function(result){
					$("tbody").html(result);
			  		bindButtonClick();
		    	}
			});
		}
	});
</script>

<?php include_once 'views/layout/'.$this->layout.'footer.php'; ?>