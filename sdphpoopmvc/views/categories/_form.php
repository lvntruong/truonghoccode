<?php 
  $params = (isset($this->record))? array('id'=>$this->record['id']):'';
?>
<form class="form-horizontal" method="post" enctype="multipart/form-data" action="<?php echo html_helpers::url(
		array('ctl'=>'categories', 
			  'act'=>$this->action, 
			  'params'=>$params
)); ?>">
  <div class="form-group">
    <label for="fullname" class="col-sm-2 control-label">Brand</label>
    <div class="col-sm-10">
      <input name="data[<?php echo $this->controller; ?>][name_category]" type="text" class="form-control" id="name_category" placeholder="name_brand" <?php echo (isset($this->record))? "value='".$this->record['name_category']."'":""; ?>>
    </div>
  </div>
  
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button name="btn_submit" type="submit" class="btn btn-default submit"><?php echo ucwords($this->action); ?></button>
    </div>
  </div>
</form>
<script src="media/js/form.js"></script>