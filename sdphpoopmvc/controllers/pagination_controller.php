<?php
main_controller::checkLogin();
class pagination_controller extends main_controller
{
	//public $components = array('SimpleImage');

	public function edit($id) 
	{
		$pagination = new pagination_model();
		$record = $pagination->getAllRecords();
		$record = mysqli_fetch_assoc($record); 
		$this->setProperty('record',$record);
		if(isset($_POST['btn_submit'])) {
			$productData = $_POST['data'][$this->controller];
			if(!empty($productData['products']))  {
				if($pagination->editRecordNoId($this->controller, $productData));
					// header( "Location: ".html_helpers::url(array('ctl'=>'products')));
			}
		}
		$this->display();
	}

}
?>
