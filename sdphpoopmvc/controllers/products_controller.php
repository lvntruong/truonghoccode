<?php
// main_controller::checkLogin();
class products_controller extends backend_controller
{
	//public $components = array('SimpleImage');
	
	public function index() 
	{
		$products = new product_model();
		$records = $products->getAllRecordsJoin('*',' LEFT JOIN (SELECT id AS id_cat, name_category FROM categories) AS cat ON id_category = id_cat');
		// echo $products->getSumRecords();
		// $records = $products->getAllRecords();
		//include_once "views/".$this->controller."/".$this->action.".php";
		$this->setProperty('records',$records);
		$this->display();
	}

	public function getby($id) 
	{
		$products = new product_model();
		// $options = array ('conditions' => 'id_category = '.$id);
		// $records = $products->getAllRecords('*',$options);
		$records = $products->getAllRecordsJoin('*',' LEFT JOIN (SELECT id AS id_cat, name_category FROM categories) AS cat ON id_category = id_cat WHERE id_category = '.$id);
		$this->setProperty('records',$records);
		$this->display();
	} 

	public function page($id) 
	{
		$pagination = new pagination_model();
		$row_pagination = mysqli_fetch_array($pagination->getAllRecords());
		$row_products = $row_pagination['products'];
		$products = new product_model();
		$records = $products->getAllRecordsJoin('*',' LEFT JOIN (SELECT id AS id_cat, name_category FROM categories) AS cat ON id_category = id_cat LIMIT '.(($id-1)*$row_products).','.$row_products);
		$this->setProperty('records',$records);
		$this->display();
	}


	public function add() 
	{
		if(isset($_POST['btn_submit'])) {
			$productData = $_POST['data'][$this->controller];
			if(!empty($productData['name']))  {
				$productData['photo'] = $this->uploadImg($_FILES);
				$product = new product_model();
				if($product->addRecord($productData));
					// header( "Location: ".html_helpers::url(array('ctl'=>'products')));
					$record = $product->getLastRecord("id, photo");
					echo json_encode($record);
			}
		} else {
			$this->display();
		}
	}

	public function edit($id) 
	{
		$product = new product_model();
		$record = $product->getRecord($id);
		$this->setProperty('record',$record);
		if(isset($_POST['btn_submit'])) {
			$productData = $_POST['data'][$this->controller];
			if(!empty($productData['name']))  {
				if(isset($_FILES) and $_FILES["image"]["name"]) {
					if(file_exists(RootURI."/media/upload/" .$this->controller.'/'.$record['photo']))
						unlink(RootURI."/media/upload/" .$this->controller.'/'.$record['photo']);
					$productData['photo'] = $this->uploadImg($_FILES);
				}
				if($product->editRecord($id, $productData))
					// header( "Location: ".html_helpers::url(array('ctl'=>'products')));
					$record = $product->getRecord($id, "photo");
					echo json_encode($record);
			}
		}  else {
		$this->display();
		}
	}
	
	public function view($id) 
	{
		$product = new product_model();
		$record = $product->getRecord($id);
		// $this->setProperty('record',$record);
		// $this->display();
		$record = json_encode($record);
		die($record);
	}
	
	public function del($id) 
	{
		$product = new product_model();
		$record = $product->getRecord($id);
		if(file_exists(RootURI."/media/upload/" .$this->controller.'/'.$record['photo']))
			unlink(RootURI."/media/upload/" .$this->controller.'/'.$record['photo']);
			
		echo $product->delRecord($id);
		exit();
		//$product->delRecord($id);
		//header( "Location: ".html_helpers::url(array('ctl'=>'products')));
	}
}
?>
