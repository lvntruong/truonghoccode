import React, { Component } from 'react';
import './App.css'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deadline: 'December 25, 2017',
      couter: 0
    };
  }

  changeDeadline(){
    this.setState({deadline: 'November 25, 2017'});
  }

  render() {
    return (
      <div className="App">
        <div className="App-title">
         Countdown to {this.state.deadline}
        </div>
        <div>
          <div className="Clock-days Clock">14 days</div>
          <div className="Clock-hours Clock">10 hours</div>
          <div className="Clock-mins Clock">15 minites</div>
          <div className="Clock-secs Clock">20 seconds</div>
        </div>
        <div>
          <input placeholder='new date'/>
          <button onClick={() => this.changeDeadline()}>
            Submit
          </button>
        </div>
      </div>
    )
  }
}

export default App;